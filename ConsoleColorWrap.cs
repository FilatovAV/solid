﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID
{
    class ConsoleColorWrap: IUI
    {
        private readonly IUI _console;

        public ConsoleColorWrap(IUI console)
        {
            _console = console;
        }

        public void ReadKey()
        {
            _console.ReadKey();
        }

        public string ReadLine()
        {
            return _console.ReadLine();
        }

        public void Write(string message)
        {
            _console.Write(message);
        }

        public void WriteLine(string message)
        {
            var cinx = 0;
            foreach (var item in message)
            {
                cinx = cinx > 14 ? 1 : ++cinx;
                Console.ForegroundColor = (ConsoleColor)cinx;
                Write(item.ToString());
            }
            _console.WriteLine(string.Empty);
        }
    }
}
