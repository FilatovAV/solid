﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOLID.Utilities
{
    class StringToUnicodeWorker : IWorker
    {
        public string DoWork(string prepareString)
        {
            return string.Join(string.Empty, prepareString.Select(s => (short)s));
        }
    }
}
