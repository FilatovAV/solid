﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOLID.Utilities
{
    class StringToUnicodeSeparateWorker : IWorker
    {
        public string DoWork(string prepareString)
        {
            return string.Join(", ", prepareString.Select(s => (short)s));
        }
    }
}
