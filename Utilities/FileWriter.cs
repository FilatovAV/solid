﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SOLID.Utilities
{
    public class FileWriter: IDataWriter
    {
        private readonly string _fileName;

        public FileWriter(string fileName)
        {
            _fileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
            File.CreateText(fileName).Close();
        }

        public void Write(string content)
        {
            File.WriteAllText(_fileName, content);
        }

        public void WriteLine(string content)
        {
            File.AppendAllText(_fileName, content);
        }
    }
}
