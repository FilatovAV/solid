﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOLID
{
    class UnicodeWorker
    {
        private readonly IUI _console;
        private readonly IDataWriter _dataWritter;
        private readonly IWorker _worker;

        public UnicodeWorker(IUI console, IDataWriter dataWritter, IWorker worker)
        {
            _console = console ?? throw new ArgumentNullException(nameof(console));
            _dataWritter = dataWritter ?? throw new ArgumentNullException(nameof(dataWritter));
            _worker = worker ?? throw new ArgumentNullException(nameof(worker));
        }
        public void Do()
        {
            Initialize();
            var result = Work();
            Result(result);
        }
        private void Initialize()
        {
            _console.WriteLine("Введите символы для конвертации в Юникоды: ");
        }
        private string Work()
        {
            var prepareString = _console.ReadLine();
            return _worker.DoWork(prepareString);
        }
        private void Result(string resultString)
        {
            _dataWritter.WriteLine(resultString);
            _console.ReadKey();
        }
    }
}
