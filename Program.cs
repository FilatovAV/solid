﻿using SOLID.Interfaces;
using SOLID.Utilities;
using System;

namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            //Single Responsobility, принцип единственной ответственности (разнесли ответственность по классам).
            //Open Closed Principle, сущности открыты для расширения, но закрыты для изменения. Создали интерфейс IWorker который может предоставлять разную реализацию выполнения задачи. И создали две разные реализации.
            //Liskov Substitution Principle, принцип подстановки Барбары Лисков. Не изменяем функциональность существующих методов, путем переопределения. Наследуемся от интерфейсов, если хотим использовать реализацию существующего класса принимаем его в конструктор (Композиция).
            //Interface Segregation Principle, принцип разделения интерфейсов (разделили интерфейсы по своему функционалу).
            //Dependency Inversion, инверсия зависимостей, абстракции не должны зависеть от сущности/реализации, реализация/сущность должна зависеть от абстракции (создали для классов интерфейсы).

            //IUI console = new ConsoleWrap();
            IUI console = new ConsoleColorWrap(new ConsoleWrap());

            IDataWriter dataWritter = new FileWriter("unicode.txt");
            //IWorker stringToUnicode = new StringToUnicodeWorker();
            IWorker stringToUnicode = new StringToUnicodeSeparateWorker();

            UnicodeWorker worker = new UnicodeWorker(console, dataWritter, stringToUnicode);
            worker.Do();
        }
    }
}
