﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.Interfaces
{
    interface IWorker
    {
        string DoWork(string prepareString);
    }
}
