﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.Interfaces
{
    interface IUI: IDataReader, IDataWriter
    {
        public string Input(string message)
        {
            Write(message);
            return ReadLine();
        }
    }
}
