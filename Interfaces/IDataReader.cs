﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.Interfaces
{
    interface IDataReader
    {
        string ReadLine();
        void ReadKey();
    }
}
