﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.Interfaces
{
    interface IDataWriter
    {
        void Write(string message);
        void WriteLine(string message);
    }
}
